$(document).ready(function(){
  let button = $('.js-navbar__button');
  let modal = $('.js-callBack');
  let close = $('.js-close');

  button.on('click', function(){
    modal.addClass('callBack--active');
  });

  close.on('click', function(){
    modal.removeClass('callBack--active');
  })

});