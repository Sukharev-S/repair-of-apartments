
new WOW().init(); // Initialization WOW.js

$(document).ready(function () {

  // Form validation
  let obj_validate = {
    rules: {
      username: { required: true, minlength: 2},
      phone: { required: true}
    },
    messages: {
      username: "Укажите имя",
      phone: "Нам нужен ваш телефон, чтобы перезвонить"
    }
  }
  $('.js-brif__form').validate(obj_validate);
  $('.js-offer__form').validate(obj_validate);
  $('.js-callBack__form').validate(obj_validate);

  // Tel mask
  $('.js-phone').mask("+7 (999) 999-9999");

  // Processing and submitting the form through AJAX
  function submit(event){
    event.preventDefault();
    $.ajax({
      url: $(this).attr('action'),
      type: 'GET',
      data: $(this).serialize(),
      success: function (response) {
        console.log('Данные успешно обработаны!');
        $('.js-name').val(''); 
        $('.js-phone').val('');
        setTimeout(() => {$('.validate-message').hide(1000)}, 3000);
      },
      error: function (err, textStatus) {
        console.log('Возникла проблема при отправке формы!');
        console.log(err + ': ' + textStatus);
      }
    });
  }
  $('.js-offer__form').on('submit', function(event){
    if ($('.js-offer__form .js-name').hasClass('valid') && $('.js-offer__form .js-phone').hasClass('valid')) {
      $('.validate-message').show(1000);
      submit(event);
    }
  });
  $('.js-brif__form').on('submit', function(event){
    if ($('.js-brif__form .js-name').hasClass('valid') && $('.js-brif__form .js-phone').hasClass('valid')) {
      $('.validate-message').show(1000);
      submit(event);
    }
  });
  $('.js-callBack__form').on('submit', function(event){
    if ($('.js-callBack__form .js-name').hasClass('valid') && $('.js-callBack__form .js-phone').hasClass('valid')) {
      $('.validate-message').show(1000);
      submit(event);
    }
  });

  // Slider's script
  $('.slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: $('.arrows__left'),
    nextArrow: $('.arrows__right'),
    responsive: [{
        breakpoint: 1224,
        settings: {slidesToShow: 2, slidesToScroll: 1}
      },
      {
        breakpoint: 1024,
        settings: {slidesToShow: 1, slidesToScroll: 1}
      }]
  });
});